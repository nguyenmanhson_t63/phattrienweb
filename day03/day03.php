<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký</title>
    <meta charset="utf-8">
    <style>
        * {
            padding: 0;
            margin: 0;
        }
        body {
            margin: 50px auto;
            text-align: center;
            width: 800px;
        }
        label {
            width: 120px;
            display: inline-block;
            text-align: center;
            font-size: 1.1rem;
            font-family: 'Times New Roman';
            background: #87cefa;
            margin-right: 20px;
            padding: 9px;
        }

        .label1 {
            width: 5px;
            background: #ffffff; 
            color: #ffffff;
        }

        input {
            border: 2px solid #8fb7e4;
            font-size: 1.5rem;
            font-weight: 100;
            font-family: 'Times New Roman';
            padding: 5px;
            width: 200px;
        }

        .input1 {
            width: 120px;
            height: 39px;
            font-size: 1.1rem;
            background: green;
            border: 2px solid #000000;

        }
        form {
            margin: 25px auto;
            padding: 10px;
            border: 2px solid #8fb7e4;
            width: 459px;
        }
        .css  {
            margin: 20px 0;
        }
        
        .button1 {
            width: 210px;
            padding: 8px;
            font-size: 1rem;
            font-family: 'Times New Roman';
            font-weight: 1;
            background: #ffffff;
            border-radius: 5px;
           
        }
        

    </style>
</head>
<body>
  
    
    <form method="post" action="register" name="Đăng ký">
        <div class="css">
            <label class="text">Họ và tên</label>
            <input class="input" type="text" name="HoVaTen">
        </div>
        <div class="css">
            <label class="text">Giới tính</label>
                <?php
                $sName = array("Nữ","Nam");
                $sValue = array("1","0");
                for ($i = 0; $i <  count($sName); $i++)  {
                    echo '<input style="width: 37px" type="radio" value="'.$sValue[$i].'">'.$sName[$i];
                    echo '<label class="label1"></label>';
                }
                
                ?>
        </div>
        <div class="css">
            <label class="text">Phân Khoa</label>
            <select class="button1" name="khoa">
                <option> </option>
                <?php
                $kName = array("Khoa học máy tính", "Khoa học vật liệu");
                $kValue = array("MAT","KDL");
                for ($i = 0; $i <  count($kName); $i++)  {
                    echo '<option value='.$kValue[$i].'>'.$kName[$i].'</option>';
                }
               
                ?>
            
            </select>
        </div>
        <input type="submit" value="Đăng ký" class="input1">
    </form> 
  
</body>
</html>