<!DOCTYPE html>
<?php 
    session_start();
    include('connectDB.php');
?>
<html>
<head>
    <title>Do Register Form</title>
    <meta charset="utf-8">
    <style>
        * {
            padding: 0;
            margin: 0;
        }
        body {
            margin: 50px auto;
            text-align: center;
            width: 800px;
        }
        label {
            border: 1px solid #8fb7e4;
            width: 120px;
            display: inline-block;
            text-align: center;
            font-size: 1.1rem;
            font-family: 'Times New Roman';
            background: #87cefa;
            margin-right: 20px;
            padding: 9px;
        }

        .labelOut {
            border: 1px solid #ffffff;
            width: 200px;
            display: inline-block;
            text-align: left;
            font-size: 1.1rem;
            font-family: 'Times New Roman';
            background: #ffffff;
            margin-right: 20px;
            padding: 9px;
        }


        form {
            margin: 25px auto;
            padding: 10px;
            border: 2px solid #8fb7e4;
            width: 459px;
        }

        .input1 {
            width: 120px;
            height: 39px;
            font-size: 1.3rem;
            background: #87cefa;
            border: 2px solid #8fb7e4;
            margin: 20px 0px 20px 0px;

        }

        .css  {
            margin: 20px 0;
        }
        
    </style>
</head>
<body>
    <form action="success.php">  
        <?php
            error_reporting(0);
            $hoten = $_SESSION["hoten"];

            if($_SESSION["gioitinh"] == "Nữ"){
                $gioitinh = '0';
            } else {
                $gioitinh = '1';
            }

            if($_SESSION["phankhoa"] == "Khoa học máy tính"){
                $phankhoa = 'MAT';
            } else {
                $phankhoa = 'KDL';
            }

            $ngaysinh = $_SESSION["ngaysinh"];
            $diachi = $_SESSION["diachi"];
            $hinhanh = $_SESSION["hinhanh"];

            $sql="insert into student(name,gender,faculty,birthday,address,avartar) value( :hten,:gtinh,:pkhoa,:nsinh,:dchi,:hanh)";
            $query=$connect->prepare($sql);
            $query->bindParam(':hten',$hoten, PDO::PARAM_STR);
            $query->bindParam(':gtinh',$gioitinh, PDO::PARAM_INT);
            $query->bindParam(':pkhoa',$phankhoa, PDO::PARAM_STR);
            $query->bindParam(':nsinh',$ngaysinh, PDO::PARAM_STR);
            $query->bindParam(':dchi',$diachi, PDO::PARAM_STR);
            $query->bindParam(':hanh',$hinhanh, PDO::PARAM_STR);
                    
            $query->execute();
        ?>  
        <div class="css">
            <label class="text">Họ và tên</label>
            <label class="labelOut" type="text" name="HoVaTen1">
                <?php 
                    echo $_SESSION["hoten"];
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Giới tính</label>
            <label class="labelOut" type="text" name="GioiTinh">
                <?php 
                    if(isset($_SESSION["gioitinh"])){
                        echo $_SESSION["gioitinh"];  
                    }
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Phân Khoa</label>
            <label class="labelOut" type="text" name="phan_khoa">
                <?php 
                        echo $_SESSION["phankhoa"]; 
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Ngày sinh</label>
            <label class="labelOut" type="text" name="ngaysinh">
                <?php 
                        echo $_SESSION["ngaysinh"]; 
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Địa chỉ</label>
            <label class="labelOut" type="text" name="diachi">
                <?php
                    if(isset($_SESSION["diachi"])){
                        echo $_SESSION["diachi"];  
                    }
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Hình ảnh</label>
            <label class="labelOut" type="text" name="diachi">
            <?php
                if ($_SESSION["hinhanh"] != "") {
                    echo "<br> <img src='upload/".$_SESSION["hinhanh"]."' width='100' height='60'>";
                }
            ?>
            </label>
        </div>
        <input type="submit" name="btn_Xacnhan" value="Xác nhận" class="input1">
    </form>
</body>
</html>