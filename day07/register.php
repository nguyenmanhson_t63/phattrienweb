<!DOCTYPE html>
<?php
session_start();
?>
<html>

<head>
    <title>Register Form</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <meta charset="utf-8">
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        body {
            margin: 50px auto;
            text-align: center;
            width: 800px;
        }

        label {
            border: 1px solid #8fb7e4;
            width: 120px;
            display: inline-block;
            text-align: center;
            font-size: 1.1rem;
            font-family: 'Times New Roman';
            background: #87cefa;
            margin-right: 20px;
            padding: 9px;
        }

        .labelNhap {
            width: 300px;
            background: #ffffff;
            color: #ff0000;
            border: 1px solid #ffffff;
            text-align: left;
            font-size: 1.1rem;
        }

        .rong1 {
            width: 6px;
            background: #ffffff;
            color: #ffffff;
            border: 1px solid #ffffff;
        }



        input {
            border: 1px solid #8fb7e4;
            font-size: 0.8rem;
            font-weight: 100;
            font-family: 'Times New Roman';
            padding: 6px;
            width: 200px;
        }

        .input1 {
            width: 120px;
            height: 39px;
            font-size: 1.3rem;
            background: #87cefa;
            border: 2px solid #8fb7e4;
            margin: 20px 0px 20px 0px;

        }

        .input2 {
            border: 1px solid #8fb7e4;
            font-size: 1.3rem;
            font-weight: 100;
            font-family: 'Times New Roman';
            padding: 6px;
            width: 152px;
        }

        form {
            margin: 25px auto;
            padding: 10px;
            border: 2px solid #8fb7e4;
            width: 459px;
        }

        .formAll {
            margin: 25px auto;
            padding: 10px;
            border: 2px solid #8fb7e4;
            width: 459px;
        }

        .css {
            margin: 20px 0;
        }

        .button1 {
            width: 167px;
            padding: 8px;
            font-size: 1rem;
            font-family: 'Times New Roman';
            font-weight: 1;
            background: #ffffff;
            border-radius: 5px;
            border: 1px solid #8fb7e4;
        }

        .not_empty {
            color: red;
        }

        .inputImage {
            border: none;
        }
    </style>
</head>

<body>

    <form method="post" action='register.php' enctype="multipart/form-data">
        <div>
            <label class="labelNhap" type="text">
                <?php
                function isValid($date, $format = 'Y-m-d')
                {
                    $dt = DateTime::createFromFormat($format, $date);
                    return $dt && $dt->format($format) === $date;
                }
                error_reporting(0);
                if (isset($_POST["btn_DangKi"])) {
                    $hoten = $_POST['hoten'];
                    $gioitinh = $_POST['gioitinh'];
                    $phankhoa = $_POST['phankhoa'];
                    $ngaysinh = $_POST['ngaysinh'];
                    $diachi = $_POST['diachi'];

                    $hoten = strip_tags($hoten);
                    $hoten = addslashes($hoten);

                    $gioitinh = strip_tags($gioitinh);
                    $gioitinh = addslashes($gioitinh);

                    $phankhoa = strip_tags($phankhoa);
                    $phankhoa = addslashes($phankhoa);

                    $ngaysinh = strip_tags($ngaysinh);
                    $ngaysinh = addslashes($ngaysinh);

                    $diachi = strip_tags($diachi);
                    $diachi = addslashes($diachi);

                    $hinhanh = strip_tags($hinhanh);
                    $hinhanh = addslashes($hinhanh);

                    if ($hoten == '') {
                        echo "Hãy nhập tên. <br>";
                    }
                    if ($gioitinh == '') {
                        echo "Hãy chọn giới tính. <br>";
                    }
                    if ($phankhoa == '') {
                        echo "Hãy chọn phân khoa. <br>";
                    }
                    if ($ngaysinh == '') {
                        echo "Hãy chọn ngày sinh. <br>";
                    } else if (isValid($ngaysinh)) {
                        echo 'Hãy chọn ngày sinh đúng định dạng dd/mm/yy. <br>';
                    }
                    if($_FILES['hinhanh']['name'] != 'image/png' ||
                        $_FILES['hinhanh']['name'] != 'image/gif' ||
                        $_FILES['hinhanh']['name'] != 'image/jpeg'
                    ) {
                        echo 'Hãy chọn đúng định dạng file ảnh ('.'png, gif, jpeg'.')';
                    }
                    if ($hoten != '' && $gioitinh != '' && $phankhoa != '' && $ngaysinh != '' &&
                        ($_FILES['hinhanh']['name'] == 'image/png' ||
                        $_FILES['hinhanh']['name'] == 'image/gif' ||
                        $_FILES['hinhanh']['name'] == 'image/jpeg')
                    ) {
                        $_SESSION['hoten'] = $hoten;
                        $_SESSION['gioitinh'] = $gioitinh;
                        $_SESSION['phankhoa'] = $phankhoa;
                        $_SESSION['ngaysinh'] = $ngaysinh;
                        $_SESSION['diachi'] = $diachi;
                        $date = (new DateTime())->format('YmdHis');
                        $hinhanhbase = basename($_FILES['hinhanh']['name']);
                        $hinhanh = str_replace('.', '_'.$date.'.', $hinhanhbase);
                        $_SESSION['hinhanh'] = $hinhanh;
                        $target_dir = "upload/";
                        if(!file_exists($target_dir)){
                            mkdir($target_dir, 0777, true);
                        };
                        $target_file = $target_dir.$hinhanh;
                        move_uploaded_file($_FILES['hinhanh']['tmp_name'], $target_file);
                        header('Location: do_regist.php');
                    }
                }
                ?>
            </label>
        </div>
        <div class="css">
            <label class="text">Họ và tên <i class="not_empty">*</i></label>
            <input class="input" type="text" name="hoten">
        </div>
        <div class="css">
            <label class="text">Giới tính <i class="not_empty">*</i></label>
            <?php
            $sName = array("Nữ", "Nam");
            $sValue = array("1", "0");
            for ($i = 0; $i <  count($sName); $i++) {
                echo '<input style="width: 36px" id="Sex" name="gioitinh" type="radio" value="' . $sName[$i] . '">' . $sName[$i];
                echo '<label class="rong1"></label>';
            }

            ?>
        </div>
        <div class="css">
            <label class="text">Phân Khoa <i class="not_empty">*</i></label>
            <select class="button1" name="phankhoa">

                <?php
                $kName = array("", "Khoa học máy tính", "Khoa học vật liệu");
                $kValue = array("", "MAT", "KDL");
                foreach ($kName as $k1) {
                    echo '<option>' . $k1 . '</option>';
                }
                ?>

            </select>
            <label class="rong1"></label>
        </div>

        <div>
            <label class="text">Ngày sinh <i class="not_empty">*</i></label>
            <input class="form-control" name="ngaysinh" type="text" id="date-time" placeholder="dd/mm/yyyy">
        </div>

        <div class="css">
            <label class="text">Địa chỉ</label>
            <input class="input" type="text" name="diachi">
        </div>

        <div class="css">
            <label class="text">Hình ảnh</label>
            <input class="inputImage" type="file" name="hinhanh" accept="image/png, image/gif, image/jpeg">
        </div>

        <input type="submit" name="btn_DangKi" value="Đăng ký" class="input1">

    </form>
</body>

</html>
<script type="text/javascript">
    $('#date-time').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy"
    });
</script>