<!DOCTYPE html>
<?php
session_start();
?>
<html>
<head>
    <title>Test Exams</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <form method="post" enctype="multipart/form-data">
        <?php
            $score = 0;
            $true_answer = array(
                "answer1" => "A",
                "answer2" => "C",
                "answer3" => "C",
                "answer4" => "B",
                "answer5" => "D",
                "answer6" => "A",
                "answer7" => "B",
                "answer8" => "C",
                "answer9" => "D",
                "answer10" => "A",
            );

            if ($_COOKIE['answer1'] == $true_answer['answer1']){
                $score++;
            }
            if ($_COOKIE['answer2'] == $true_answer['answer2']){
                $score++;
            }
            if ($_COOKIE['answer3'] == $true_answer['answer3']){
                $score++;
            }
            if ($_COOKIE['answer4'] == $true_answer['answer4']){
                $score++;
            }
            if ($_COOKIE['answer5'] == $true_answer['answer5']){
                $score++;
            }
            if (isset($_POST['answer6'])) {
                if ($_POST['answer6'] == $true_answer['answer6']){
                    $score++;
                }
            }
            if (isset($_POST['answer7'])) {
                if ($_POST['answer7'] == $true_answer['answer7']){
                    $score++;
                }
            }
            if (isset($_POST['answer8'])) {
                if ($_POST['answer8'] == $true_answer['answer8']){
                    $score++;
                }
            }
            if (isset($_POST['answer9'])) {
                if ($_POST['answer9'] == $true_answer['answer9']){
                    $score++;
                }
            }
            if (isset($_POST['answer10'])) {
                if ($_POST['answer10'] == $true_answer['answer10']){
                    $score++;
                }
            }

        ?>
        <h1 class="h1">Result</h1>
        <?php
            echo "Bạn đã được ".$score." điểm.</br>";
            if ($score < 4) {
                echo "Bạn quá kém, cần ôn tập thêm";
            } else if ($score >= 4 && $score <= 7) {
                echo "Cũng bình thường";
            } else {
                echo "Sắp sửa làm được trợ giảng lớp PHP";
            }
        ?>
        <br>
        <br>

        <h1 class="h1">Detail</h1>
        <!-- 1 -->
        <p class="pq">1) 1 + 1 = ?</p>
        <input type="radio" id="A-1" name="answer1" value="A" checked>
        <label class="true_answer">A. 2</label><br>
        <input type="radio" id="B-1" name="answer1" value="B">
        <label>B. 3</label><br>
        <input type="radio" id="C-1" name="answer1" value="C">
        <label>C. 4</label><br>
        <input type="radio" id="D-1" name="answer1" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 2 -->
        <p class="pq">2) 2 + 2 = ?</p>
        <input type="radio" id="A-2" name="answer2" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-2" name="answer2" value="B" checked>
        <label class="fail_answer">B. 3</label><br>
        <input type="radio" id="C-2" name="answer2" value="C">
        <label class="true_answer">C. 4</label><br>
        <input type="radio" id="D-2" name="answer2" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 3 -->
        <p class="pq">3) 3 + 3 = ?</p>
        <input type="radio" id="A-3" name="answer3" value="A">
        <label>A. 9</label><br>
        <input type="radio" id="B-3" name="answer3" value="B">
        <label>B. 10</label><br>
        <input type="radio" id="C-3" name="answer3" value="C" checked>
        <label class="true_answer">C. 6</label><br>
        <input type="radio" id="D-3" name="answer3" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 4 -->
        <p class="pq">4) 4 + 4 = ?</p>
        <input type="radio" id="A-4" name="answer4" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-4" name="answer4" value="B">
        <label class="true_answer">B. 8</label><br>
        <input type="radio" id="C-4" name="answer4" value="C">
        <label>C. 19</label><br>
        <input type="radio" id="D-4" name="answer4" value="D" checked>
        <label class="fail_answer">D. 5</label><br>
        <hr><br>

        <!-- 5 -->
        <p class="pq">5) 5 + 5 = ?</p>
        <input type="radio" id="A-5" name="answer5" value="A" checked>
        <label class="fail_answer">A. 2</label><br>
        <input type="radio" id="B-5" name="answer5" value="B">
        <label>B. 3</label><br>
        <input type="radio" id="C-5" name="answer5" value="C">
        <label>C. 4</label><br>
        <input type="radio" id="D-5" name="answer5" value="D">
        <label class="true_answer">D. 10</label><br>
        <hr><br>

         <!-- 6 -->
         <p class="pq">6) 6 + 6 = ?</p>
        <input type="radio" id="A-6" name="answer6" value="A">
        <label class="true_answer">A. 12</label><br>
        <input type="radio" id="B-6" name="answer6" value="B" checked>
        <label class="fail_answer">B. 13</label><br>
        <input type="radio" id="C-6" name="answer6" value="C">
        <label>C. 14</label><br>
        <input type="radio" id="D-6" name="answer6" value="D">
        <label>D. 15</label><br>
        <hr><br>

        <!-- 7 -->
        <p class="pq">7) 7 + 7 = ?</p>
        <input type="radio" id="A-7" name="answer7" value="A">
        <label>A. 19</label><br>
        <input type="radio" id="B-7" name="answer7" value="B">
        <label class="true_answer">B. 14</label><br>
        <input type="radio" id="C-7" name="answer7" value="C" checked>
        <label class="fail_answer">C. 16</label><br>
        <input type="radio" id="D-7" name="answer7" value="D">
        <label>D. 15</label><br>
        <hr><br>

        <!-- 8 -->
        <p class="pq">8) 8 + 8 = ?</p>
        <input type="radio" id="A-8" name="answer8" value="A">
        <label>A. 12</label><br>
        <input type="radio" id="B-8" name="answer8" value="B">
        <label>B. 18</label><br>
        <input type="radio" id="C-8" name="answer8" value="C">
        <label class="true_answer">C. 16</label><br>
        <input type="radio" id="D-8" name="answer8" value="D" checked>
        <label class="fail_answer">D. 15</label><br>
        <hr><br>

        <!-- 9 -->
        <p class="pq">9) 9 + 9 = ?</p>
        <input type="radio" id="A-9" name="answer9" value="A" checked>
        <label class="fail_answer">A. 12</label><br>
        <input type="radio" id="B-9" name="answer9" value="B">
        <label>B. 13</label><br>
        <input type="radio" id="C-9" name="answer9" value="C">
        <label>C. 14</label><br>
        <input type="radio" id="D-9" name="answer9" value="D">
        <label class="true_answer">D. 18</label><br>
        <hr><br>
  
        <!-- 10 -->
        <p class="pq">10) 10 + 10 = ?</p>
        <input type="radio" id="A-10" name="answer10" value="A">
        <label class="true_answer">A. 20</label><br>
        <input type="radio" id="B-10" name="answer10" value="B" checked>
        <label class="fail_answer">B. 30</label><br>
        <input type="radio" id="C-10" name="answer10" value="C">
        <label>C. 40</label><br>
        <input type="radio" id="D-10" name="answer10" value="D">
        <label>D. 50</label><br>
        <hr><br>
    </form>
</body>
</html>

