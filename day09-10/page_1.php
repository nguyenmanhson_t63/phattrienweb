<!DOCTYPE html>
<?php
session_start();
?>
<html>
<head>
    <title>Test Exams</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <form method="post" enctype="multipart/form-data" action="page_2.php">

        <h1 class="h1">Test Exam</h1>
        <br><br>

        <!-- 1 -->
        <p class="pq">1) 1 + 1 = ?</p>
        <input type="radio" id="A-1" name="answer1" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-1" name="answer1" value="B">
        <label>B. 3</label><br>
        <input type="radio" id="C-1" name="answer1" value="C">
        <label>C. 4</label><br>
        <input type="radio" id="D-1" name="answer1" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 2 -->
        <p class="pq">2) 2 + 2 = ?</p>
        <input type="radio" id="A-2" name="answer2" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-2" name="answer2" value="B">
        <label>B. 3</label><br>
        <input type="radio" id="C-2" name="answer2" value="C">
        <label>C. 4</label><br>
        <input type="radio" id="D-2" name="answer2" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 3 -->
        <p class="pq">3) 3 + 3 = ?</p>
        <input type="radio" id="A-3" name="answer3" value="A">
        <label>A. 9</label><br>
        <input type="radio" id="B-3" name="answer3" value="B">
        <label>B. 10</label><br>
        <input type="radio" id="C-3" name="answer3" value="C">
        <label>C. 6</label><br>
        <input type="radio" id="D-3" name="answer3" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 4 -->
        <p class="pq">4) 4 + 4 = ?</p>
        <input type="radio" id="A-4" name="answer4" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-4" name="answer4" value="B">
        <label>B. 8</label><br>
        <input type="radio" id="C-4" name="answer4" value="C">
        <label>C. 19</label><br>
        <input type="radio" id="D-4" name="answer4" value="D">
        <label>D. 5</label><br>
        <hr><br>

        <!-- 5 -->
        <p class="pq">5) 5 + 5 = ?</p>
        <input type="radio" id="A-5" name="answer5" value="A">
        <label>A. 2</label><br>
        <input type="radio" id="B-5" name="answer5" value="B">
        <label>B. 3</label><br>
        <input type="radio" id="C-5" name="answer5" value="C">
        <label>C. 4</label><br>
        <input type="radio" id="D-5" name="answer5" value="D">
        <label>D. 10</label><br>
        <hr><br>

        <input type="submit" name="btn_next" value="Next" class="input1">
    </form>
</body>
</html>

