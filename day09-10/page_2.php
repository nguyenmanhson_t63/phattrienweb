<!DOCTYPE html>
<?php
session_start();
?>
<html>
<head>
    <title>Test Exams</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <?php
        // 1
        if(isset($_POST['answer1'])) {
            setcookie('answer1', $_POST['answer1'], time() + (86400 * 30), "/");
        } else {
            setcookie('answer1', 'E', time() + (86400 * 30), "/");
        }

        // 2
        if(isset($_POST['answer2'])) {
            setcookie('answer2', $_POST['answer2'], time() + (86400 * 30), "/");
        } else {
            setcookie('answer2', 'E', time() + (86400 * 30), "/");
        }

        // 3
        if(isset($_POST['answer3'])) {
            setcookie('answer3', $_POST['answer3'], time() + (86400 * 30), "/");
        } else {
            setcookie('answer3', 'E', time() + (86400 * 30), "/");
        }

        // 4
        if(isset($_POST['answer4'])) {
            setcookie('answer4', $_POST['answer4'], time() + (86400 * 30), "/");
        } else {
            setcookie('answer4', 'E', time() + (86400 * 30), "/");
        }

        // 5
        if(isset($_POST['answer5'])) {
            setcookie('answer5', $_POST['answer5'], time() + (86400 * 30), "/");
        } else {
            setcookie('answer5', 'E', time() + (86400 * 30), "/");
        }
    ?>
    <form method="post" enctype="multipart/form-data" action="result.php">
        <h1 class="h1">Test Exam</h1>
        <br>
        <br>

        <!-- 6 -->
        <p class="pq">6) 6 + 6 = ?</p>
        <input type="radio" id="A-6" name="answer6" value="A">
        <label>A. 12</label><br>
        <input type="radio" id="B-6" name="answer6" value="B">
        <label>B. 13</label><br>
        <input type="radio" id="C-6" name="answer6" value="C">
        <label>C. 14</label><br>
        <input type="radio" id="D-6" name="answer6" value="D">
        <label>D. 15</label><br>
        <hr><br>

        <!-- 7 -->
        <p class="pq">7) 7 + 7 = ?</p>
        <input type="radio" id="A-7" name="answer7" value="A">
        <label>A. 19</label><br>
        <input type="radio" id="B-7" name="answer7" value="B">
        <label>B. 14</label><br>
        <input type="radio" id="C-7" name="answer7" value="C">
        <label>C. 16</label><br>
        <input type="radio" id="D-7" name="answer7" value="D">
        <label>D. 15</label><br>
        <hr><br>

        <!-- 8 -->
        <p class="pq">8) 8 + 8 = ?</p>
        <input type="radio" id="A-8" name="answer8" value="A">
        <label>A. 12</label><br>
        <input type="radio" id="B-8" name="answer8" value="B">
        <label>B. 18</label><br>
        <input type="radio" id="C-8" name="answer8" value="C">
        <label>C. 16</label><br>
        <input type="radio" id="D-8" name="answer8" value="D">
        <label>D. 15</label><br>
        <hr><br>

        <!-- 9 -->
        <p class="pq">9) 9 + 9 = ?</p>
        <input type="radio" id="A-9" name="answer9" value="A">
        <label>A. 12</label><br>
        <input type="radio" id="B-9" name="answer9" value="B">
        <label>B. 13</label><br>
        <input type="radio" id="C-9" name="answer9" value="C">
        <label>C. 14</label><br>
        <input type="radio" id="D-9" name="answer9" value="D">
        <label>D. 18</label><br>
        <hr><br>
  
        <!-- 10 -->
        <p class="pq">10) 10 + 10 = ?</p>
        <input type="radio" id="A-10" name="answer10" value="A">
        <label>A. 20</label><br>
        <input type="radio" id="B-10" name="answer10" value="B">
        <label>B. 30</label><br>
        <input type="radio" id="C-10" name="answer10" value="C">
        <label>C. 40</label><br>
        <input type="radio" id="D-10" name="answer10" value="D">
        <label>D. 50</label><br>
        <hr><br>

        <input type="submit" name="btn_submit" value="Nộp bài" class="input1">
    </form>
</body>
</html>
